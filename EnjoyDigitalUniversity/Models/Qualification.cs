﻿namespace EnjoyDigitalUniversity.Models
{
    public class Qualification
    {
        public string Type { get; set; }

        public string Subject { get; set; }

        public string Grade { get; set; }
    }
}